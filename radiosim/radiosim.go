package radiosim

import (
	"gitlab.com/dr.sybren/mesh-network-sim/radionode"
)

type RadioSimulator struct {
	nodes []radionode.RadioNode
}

func New() radionode.RadioSimulator {
	return &RadioSimulator{
		make([]radionode.RadioNode, 0),
	}
}

func (rs *RadioSimulator) AddNode(node radionode.RadioNode) {
	rs.nodes = append(rs.nodes, node)
	node.SetRadioSimulator(rs)
}

func (rs *RadioSimulator) Broadcast(fromNode radionode.MessageSender, message radionode.Message) {
	for _, node := range rs.nodes {
		if node == fromNode {
			continue
		}
		rs.sendMessageTo(node, fromNode, message)
	}
}

func (rs *RadioSimulator) sendMessageTo(
	toNode radionode.MessageReceiver,
	fromNode radionode.MessageSender,
	message radionode.Message,
) {
	// TODO: Use locations & radio strength to determine chance of arrival.
	toNode.MessageReceived(message, fromNode)
}
