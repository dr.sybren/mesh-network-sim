package radionode

type Message interface {
	GetPayloadString() string
	GetPayload() []byte
	GetTTL() int8
}

type stringMessage struct {
	payload string
	ttl     int8
}

func StringMessage(message string, ttl int8) Message {
	return stringMessage{message, ttl}
}

func (sm stringMessage) GetPayloadString() string {
	return sm.payload
}
func (sm stringMessage) GetPayload() []byte {
	return []byte(sm.payload)
}

func (sm stringMessage) GetTTL() int8 {
	return sm.ttl
}
