package radionode

type Metres int

type Location struct {
	X, Y, Z Metres
}

func NewLocation(X, Y, Z Metres) Location {
	return Location{X, Y, Z}
}
