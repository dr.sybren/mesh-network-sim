package radionode

import "github.com/sirupsen/logrus"

type SimulatedRadioNode struct {
	name     string
	location Location

	radioStrength float32
	simulator     RadioSimulator

	logger *logrus.Entry
}

func New(name string, location Location) RadioNode {
	return &SimulatedRadioNode{
		name:          name,
		location:      location,
		radioStrength: 1.0,
		simulator:     nil,
		logger:        logrus.WithField("node", name),
	}
}

func (node *SimulatedRadioNode) SetLocation(loc Location) {
	node.location = loc
}

func (node *SimulatedRadioNode) GetLocation() Location {
	return node.location
}

func (node *SimulatedRadioNode) GetName() string {
	return node.name
}

func (node *SimulatedRadioNode) MessageReceived(message Message, sender MessageSender) {
	node.logger.WithFields(logrus.Fields{
		"ttl":     message.GetTTL(),
		"payload": message.GetPayloadString(),
		"from":    sender.GetName(),
	}).Info("message received")
}

// MessageSender interface

func (node *SimulatedRadioNode) MessageLost(message Message) {}
func (node *SimulatedRadioNode) MessageACK(message Message)  {}

func (node *SimulatedRadioNode) SetRadioSimulator(simulator RadioSimulator) {
	node.simulator = simulator
}
