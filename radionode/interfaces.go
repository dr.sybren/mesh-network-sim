package radionode

type Positionable interface {
	SetLocation(loc Location)
	GetLocation() Location
}

type MessageReceiver interface {
	Positionable
	MessageReceived(message Message, sender MessageSender)
}

type MessageSender interface {
	Positionable
	GetName() string

	MessageLost(message Message)
	MessageACK(message Message)
	SetRadioSimulator(simulator RadioSimulator)
}

type RadioNode interface {
	Positionable
	MessageReceiver
	MessageSender
}

type RadioSimulator interface {
	AddNode(node RadioNode)
	Broadcast(fromNode MessageSender, message Message)
}
