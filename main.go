package main

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of Mesh-Network-Sim.
 *
 * Mesh-Network-Sim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * Mesh-Network-Sim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mesh-Network-Sim.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"flag"
	"log"

	"github.com/sirupsen/logrus"
	"gitlab.com/dr.sybren/mesh-network-sim/radionode"
	"gitlab.com/dr.sybren/mesh-network-sim/radiosim"
)

const applicationName = "Mesh-Network-Sim"

var applicationVersion = "set-during-build"

var cliArgs struct {
	quiet bool
	debug bool
}

func parseCliArgs() {
	flag.BoolVar(&cliArgs.quiet, "quiet", false, "Disable info-level logging.")
	flag.BoolVar(&cliArgs.debug, "debug", false, "Enable debug-level logging.")
	flag.Parse()
}

func configLogging() {
	logrus.SetFormatter(&logrus.TextFormatter{
		FullTimestamp: true,
	})

	// Only log the warning severity or above by default.
	var level logrus.Level
	switch {
	case cliArgs.debug:
		level = logrus.DebugLevel
	case cliArgs.quiet:
		level = logrus.WarnLevel
	default:
		level = logrus.InfoLevel
	}
	logrus.SetLevel(level)
	log.SetOutput(logrus.StandardLogger().Writer())
}

func main() {
	parseCliArgs()
	configLogging()
	logrus.WithField("version", applicationVersion).Infof("starting %s", applicationName)

	sim := radiosim.New()

	node1 := radionode.New("node1", radionode.NewLocation(1, 0, 0))
	sim.AddNode(node1)

	node2 := radionode.New("node2", radionode.NewLocation(10, 0, 0))
	sim.AddNode(node2)

	node3 := radionode.New("node3", radionode.NewLocation(10, 0, 0))
	sim.AddNode(node3)

	sim.Broadcast(node1, radionode.StringMessage("hello", 4))

	logrus.Info("Shutting down")
}
